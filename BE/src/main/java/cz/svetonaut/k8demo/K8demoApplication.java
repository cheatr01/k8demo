package cz.svetonaut.k8demo;

import cz.svetonaut.k8demo.backend.service.QueueListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;

@SpringBootApplication
@EnableJms
public class K8demoApplication implements JmsListenerConfigurer {

    @Autowired
    private QueueListener queueListener;

    @Value("${queue.name}")
    private String queueName;

    @Value("${worker.name}")
    private String workName;

    public static void main(String[] args) {
        SpringApplication.run(K8demoApplication.class, args);
    }

    @Override
    public void configureJmsListeners(JmsListenerEndpointRegistrar jmsListenerEndpointRegistrar) {
        SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
        endpoint.setMessageListener(queueListener);
        endpoint.setDestination(queueName);
        endpoint.setId(workName);
        jmsListenerEndpointRegistrar.registerEndpoint(endpoint);
    }
}
