package cz.svetonaut.k8demo.backend.controller;

import cz.svetonaut.k8demo.backend.service.QueueListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

@Controller
public class HomeController {

    @Value("${queue.name}")
    private String queueName;

    @Value("${build.version}")
    private String version;

    private UUID uuid = UUID.randomUUID();

    @Autowired
    private QueueListener queueService;

    @ResponseBody
    @RequestMapping(value = "/metrics", produces = "text/plain")
    public String metrics() {
        int totalMessages = queueService.pendingJobs(queueName);
        return "# HELP messages Number of messages in the queueService\n"
                + "# TYPE messages gauge\n"
                + "messages " + totalMessages;
    }

    @RequestMapping(value = "/health")
    public ResponseEntity health() {
        HttpStatus status;
        if (queueService.isUp()) {
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(status);
    }

    @RequestMapping(value = "/version", produces = "text/plain")
    @ResponseBody
    public String version() {
        return "UUID: " + uuid + "; VERSION: " + version;
    }
}
