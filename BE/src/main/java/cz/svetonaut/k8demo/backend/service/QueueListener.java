package cz.svetonaut.k8demo.backend.service;

import org.apache.activemq.command.ActiveMQTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.Collections;


public class QueueListener implements MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueueListener.class);

    @Autowired
    private JmsTemplate jmsTemplate;


    @Override
    public void onMessage(Message message) {
        if (message instanceof ActiveMQTextMessage) {
            final ActiveMQTextMessage msg = (ActiveMQTextMessage) message;
            try {
                LOGGER.info("Receive message with id " + msg.getJMSMessageID() + " and with body: " + msg.getText());
                Thread.sleep(5000);
                LOGGER.info("Message with id: " + msg.getJMSMessageID() + " is done.");
            } catch (JMSException | InterruptedException e) {
                e.printStackTrace();
            }
            increaseCounter();
        } else {
            try {
                LOGGER.warn("Received message of wrong type. Id: " + message.getJMSMessageID());
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

    public void increaseCounter() {
        jmsTemplate.convertAndSend("counter", "1");
    }

    public int pendingJobs(String queueName) {
        return jmsTemplate.browse(queueName, (s, qb) -> Collections.list(qb.getEnumeration()).size());
    }

    public boolean isUp() {
        final ConnectionFactory connectionFactory = jmsTemplate.getConnectionFactory();
        try {
            connectionFactory.createConnection().close();
            return true;
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return false;
    }
}
