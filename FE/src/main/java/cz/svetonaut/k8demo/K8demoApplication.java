package cz.svetonaut.k8demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class K8demoApplication {

    public static void main(String[] args) {
        SpringApplication.run(K8demoApplication.class, args);
    }

}
