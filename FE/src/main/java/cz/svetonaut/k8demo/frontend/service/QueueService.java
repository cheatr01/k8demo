package cz.svetonaut.k8demo.frontend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import java.util.Collections;

@Service
public class QueueService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueueService.class);

    @Value("${queue.name}")
    private String queueName;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private CounterService counterService;

    public int pendingJobs() {
        int result;
        try {
            result = jmsTemplate.browse(queueName, (s, qb) -> Collections.list(qb.getEnumeration()).size());
        } catch (Exception e) {
            e.printStackTrace(); //todo logging
            result = -1;
        }
        return result;
    }

    public Long completedJobs() {
        long result;
        try {
            result = counterService.getCounter();
        } catch (Exception e) {
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    public boolean isUp() {
        final ConnectionFactory connectionFactory = jmsTemplate.getConnectionFactory();
        try {
            connectionFactory.createConnection().close();
            return true;
        } catch (JMSException e) {
            e.printStackTrace(); //todo logging
        }
        return false;
    }

    public void send(String id) {
        LOGGER.info("Sending id: " + id);
        jmsTemplate.convertAndSend(queueName, id);
    }


}
