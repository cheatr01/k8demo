package cz.svetonaut.k8demo.frontend.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;

import java.util.UUID;

@Configuration
public class JmsListenerConfig {

    private final UUID uuid = UUID.randomUUID();

    @Bean
    public JmsListenerContainerFactory<?> myTopicFactory(ActiveMQConnectionFactory connectionFactory, DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setPubSubDomain(true);
        factory.setSubscriptionDurable(true);
        factory.setClientId(uuid.toString());
        configurer.configure(factory, connectionFactory);
        return factory;
    }
}
