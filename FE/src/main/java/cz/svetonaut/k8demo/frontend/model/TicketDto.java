package cz.svetonaut.k8demo.frontend.model;

public class TicketDto {
    private long quantity;

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
}
