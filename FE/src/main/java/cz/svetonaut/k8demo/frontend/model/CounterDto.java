package cz.svetonaut.k8demo.frontend.model;

public class CounterDto {

    private Long counter;
    private int pendingJobs;

    public CounterDto(Long counter, int pendingJobs) {
        this.counter = counter;
        this.pendingJobs = pendingJobs;
    }

    public CounterDto() {
    }

    public Long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }

    public int getPendingJobs() {
        return pendingJobs;
    }

    public void setPendingJobs(int pendingJobs) {
        this.pendingJobs = pendingJobs;
    }
}
