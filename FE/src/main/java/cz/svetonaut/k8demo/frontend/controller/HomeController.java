package cz.svetonaut.k8demo.frontend.controller;

import cz.svetonaut.k8demo.frontend.model.CounterDto;
import cz.svetonaut.k8demo.frontend.model.TicketDto;
import cz.svetonaut.k8demo.frontend.service.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
public class HomeController {

    @Autowired
    private QueueService queueService;

    @Value("${queue.name}")
    private String queueName;

    @Value("${build.version}")
    private String version;

    @GetMapping("/")
    public String home(Model model) {
        final long completedJobs = queueService.completedJobs();
        final int pendingJobs = queueService.pendingJobs();
        model.addAttribute("ticket", new TicketDto());
        model.addAttribute("pendingJobs", pendingJobs == -1 ? "Nan" : pendingJobs);
        model.addAttribute("isConnected", queueService.isUp() ? "yes" : "no");
        model.addAttribute("queueName", this.queueName);
        model.addAttribute("counter",completedJobs == -1 ? "NaN" : completedJobs);
        model.addAttribute("version", version);
        return "home";
    }

    @PostMapping("/submit")
    public String submit(@ModelAttribute TicketDto ticketDto) {
        for (long i = 0; i < ticketDto.getQuantity(); i++) {
            String id = UUID.randomUUID().toString();
            queueService.send(id);
        }
        return "success";
    }

    @ResponseBody
    @RequestMapping(value = "/metrics", produces = "text/plain")
    public String metrics() {
        int totalMessages = queueService.pendingJobs();
        return "# HELP messages Number of messages in the queueService\n"
                + "# TYPE messages gauge\n"
                + "messages " + totalMessages;
    }

    @RequestMapping(value = "/health")
    public ResponseEntity health() {
        HttpStatus status;
        if (queueService.isUp()) {
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(status);
    }

    @MessageMapping("counter")
    @SendTo("topic/counter")
    public CounterDto actualizeCounter(Long counter) {
        final int pendingJobs = queueService.pendingJobs();
        return new CounterDto(counter, pendingJobs);
    }
}
