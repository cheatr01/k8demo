package cz.svetonaut.k8demo.frontend.model;

import java.util.Objects;

public class CompletedJobsDto {

    private Long counter = 0L;

    public CompletedJobsDto() {
    }

    public CompletedJobsDto(Long counter) {
        this.counter = counter;
    }

    public Long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompletedJobsDto that = (CompletedJobsDto) o;
        return Objects.equals(counter, that.counter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(counter);
    }
}
