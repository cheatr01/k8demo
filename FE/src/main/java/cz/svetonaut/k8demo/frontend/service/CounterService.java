package cz.svetonaut.k8demo.frontend.service;

import cz.svetonaut.k8demo.frontend.model.CompletedJobsDto;
import cz.svetonaut.k8demo.frontend.model.CounterDto;
import org.apache.activemq.Message;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class CounterService {

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private QueueService queueService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CounterService.class);

    private RestTemplate restTemplate = new RestTemplate();

    @Value("${counter.endpoint.url}")
    private String counterUrl;

    public Long getCounter() {
        final HttpHeaders httpHeaders = new HttpHeaders();
        final HttpEntity<Object> httpEntity = new HttpEntity<>(httpHeaders);
        final ResponseEntity<CompletedJobsDto> responseEntity = restTemplate.exchange(counterUrl, HttpMethod.GET, httpEntity, CompletedJobsDto.class);
        final CompletedJobsDto counter = responseEntity.getBody();
        return Optional.ofNullable(counter).map(CompletedJobsDto::getCounter).orElse(0L);
    }

    @JmsListener(destination = "${topic.name}", containerFactory = "myTopicFactory", subscription = "${topic.name}", id = "frontend")
    public void receiveCounter(Message message) {
        LOGGER.info("Received counter message");
        if (message instanceof ActiveMQTextMessage) {
            try {
                final String text = ((ActiveMQTextMessage) message).getText();
                Long counter = Long.parseLong(text);
                LOGGER.info("Counter changed to " + counter);
                template.convertAndSend("/topic/counter", new CounterDto(counter, queueService.pendingJobs()));
                message.acknowledge();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            LOGGER.warn("Bad format of message");
        }
    }
}
