#!/bin/bash

TAG=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec 2>/dev/null)

/bin/bash ../bin/docker-build-push-parent.sh svetonaut/k8frtd:$TAG