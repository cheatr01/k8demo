package cz.svetonaut.counter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class CounterService {

    @Autowired
    private RedisTemplate redisTemplate;

    private static final String COUNTER_KEY = "REDIS_K8_COUNTER";

    private RedisAtomicLong counter;

    private void prepareCounter() {
        try {
            counter = new RedisAtomicLong(COUNTER_KEY, redisTemplate.getConnectionFactory());
        }catch (IllegalArgumentException e){
            throw new IllegalStateException("Redis connection factory was not initialize", e);
        }
    }

    public long increaseCounter() {
        prepareCounter();
        return counter.incrementAndGet();
    }

    public Long getCounter() {
        prepareCounter();
        return counter.get();
    }

    /**
     * If RedisTemplate don't establish connection to redis, method startUp throw exception. If establish connection
     * we return true.
     * @return true if connection is up, exception if connection is closed.
     * @throws IllegalStateException if connection is closed
     */
    public boolean isUp() {
        prepareCounter();
        return true;
    }
}
