package cz.svetonaut.counter.service;

import cz.svetonaut.counter.model.CounterDto;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

@Service
@Transactional
public class JmsListener implements MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(JmsListener.class);

    @Autowired
    private CounterService counterService;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Value("${counterevent.topic.name}")
    private String counterTopicName;

    @Override
    public void onMessage(Message message) {
        LOGGER.info("Received counter message");
        if (message instanceof ActiveMQTextMessage) {
            try {
                if (((ActiveMQTextMessage) message).getText().equals("1")) {
                    final long counter = counterService.increaseCounter();
                    LOGGER.info("Counter increased");
                    jmsTemplate.setPubSubDomain(true);
                    jmsTemplate.convertAndSend(counterTopicName, String.valueOf(counter));
                }
            } catch (JMSException e) {
                e.printStackTrace();
            }
        } else {
            LOGGER.warn("Bad format of message");
        }

    }
}
