package cz.svetonaut.counter.model;


import java.util.Objects;

public class CounterDto {

    private Long counter = 0L;

    public CounterDto() {
    }

    public CounterDto(Long counter) {
        this.counter = counter;
    }

    public Long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CounterDto that = (CounterDto) o;
        return Objects.equals(counter, that.counter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(counter);
    }
}
