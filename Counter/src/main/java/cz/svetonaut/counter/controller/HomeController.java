package cz.svetonaut.counter.controller;

import cz.svetonaut.counter.model.CounterDto;
import cz.svetonaut.counter.service.CounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    @Autowired
    private CounterService counterService;

    @ResponseBody
    @RequestMapping(value = "/health", produces = "text/plain")
    public ResponseEntity health() {
        HttpStatus status;
        try {
            counterService.isUp();
        } catch (Exception e) {
            e.printStackTrace();
            status = HttpStatus.BAD_REQUEST;
        }
        status = HttpStatus.OK;
        return new ResponseEntity<>(status);
    }

    @GetMapping(path = "/jobs", produces = "application/json")
    @ResponseBody
    public ResponseEntity<CounterDto> getCompletedJobs() {
        final Long counter = counterService.getCounter();
        return new ResponseEntity<CounterDto>(new CounterDto(counter), HttpStatus.OK);
    }
}
