package cz.svetonaut.counter;

import cz.svetonaut.counter.service.JmsListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;

@SpringBootApplication
@EnableJms
public class CounterApplication implements JmsListenerConfigurer {

    @Value("${queue.name}")
    private String queueName;

    @Value("${worker.name}")
    private String workName;

    @Autowired
    private JmsListener jmsListener;

    public static void main(String[] args) {
        SpringApplication.run(CounterApplication.class, args);
    }

    @Override
    public void configureJmsListeners(JmsListenerEndpointRegistrar jmsListenerEndpointRegistrar) {
        SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
        endpoint.setMessageListener(jmsListener);
        endpoint.setDestination(queueName);
        endpoint.setId(workName);
        jmsListenerEndpointRegistrar.registerEndpoint(endpoint);
    }

}
