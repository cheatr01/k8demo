#!/bin/bash

IMAGE_NAME=$1

docker login
docker build -t $IMAGE_NAME .
docker push $IMAGE_NAME