#!/bin/bash

cd ../BE/ || exit 1
/bin/bash ./docker-build-push.sh

cd ../Counter || exit 2
/bin/bash ./docker-build-push.sh

cd ../FE || exit 3
/bin/bash ./docker-build-push.sh